NAME mip-partition
* Max problem is converted into Min one
ROWS
 N  OBJ
 E  R0      
 E  R1      
 E  R2      
 E  R3      
 E  R4      
 E  R5      
 E  R6      
 E  R7      
 G  R8      
 G  R9      
 L  R10     
 L  R11     
 G  R12     
 G  R13     
 G  R14     
 G  R15     
 L  R16     
 L  R17     
 L  R18     
 L  R19     
 L  R20     
 L  R21     
 L  R22     
 L  R23     
 L  R24     
 L  R25     
 L  R26     
 L  R27     
 L  R28     
 L  R29     
 L  R30     
 L  R31     
 L  R32     
 L  R33     
 L  R34     
 L  R35     
 L  R36     
 L  R37     
 L  R38     
 L  R39     
 L  R40     
 L  R41     
 L  R42     
 L  R43     
 L  R44     
 L  R45     
 L  R46     
 L  R47     
 L  R48     
 L  R49     
 L  R50     
 L  R51     
 L  R52     
 L  R53     
 L  R54     
 L  R55     
 L  R56     
 L  R57     
 L  R58     
 L  R59     
 L  R60     
 L  R61     
 L  R62     
 L  R63     
 L  R64     
 L  R65     
 L  R66     
 L  R67     
 L  R68     
 L  R69     
 L  R70     
 L  R71     
 L  R72     
 L  R73     
 L  R74     
 L  R75     
 L  R76     
 L  R77     
 L  R78     
 L  R79     
 L  R80     
 L  R81     
 L  R82     
 L  R83     
 L  R84     
 L  R85     
 L  R86     
 L  R87     
 L  R88     
 L  R89     
 L  R90     
 L  R91     
 L  R92     
 L  R93     
 L  R94     
 L  R95     
 L  R96     
 L  R97     
 L  R98     
 L  R99     
 L  R100    
 L  R101    
 L  R102    
 L  R103    
 E  R104    
 E  R105    
 E  R106    
 E  R107    
 E  R108    
 E  R109    
 E  R110    
 E  R111    
COLUMNS
    MARKER    'MARKER'                 'INTORG'
    x[0,0]    R0        1
    x[0,0]    R8        1
    x[0,0]    R10       1
    x[0,0]    R20       -1
    x[0,0]    R32       -1
    x[0,0]    R44       -1
    x[0,0]    R104      1
    x[0,1]    R0        1
    x[0,1]    R9        1
    x[0,1]    R11       1
    x[0,1]    R21       -1
    x[0,1]    R33       -1
    x[0,1]    R45       -1
    x[0,1]    R104      1
    x[0,2]    R0        1
    x[0,2]    R12       1
    x[0,2]    R16       1
    x[0,2]    R22       -1
    x[0,2]    R34       -1
    x[0,2]    R46       -1
    x[0,3]    R0        1
    x[0,3]    R13       1
    x[0,3]    R17       1
    x[0,3]    R23       -1
    x[0,3]    R35       -1
    x[0,3]    R47       -1
    x[0,4]    R0        1
    x[0,4]    R14       1
    x[0,4]    R18       1
    x[0,4]    R24       -1
    x[0,4]    R36       -1
    x[0,4]    R48       -1
    x[0,5]    R0        1
    x[0,5]    R15       1
    x[0,5]    R19       1
    x[0,5]    R25       -1
    x[0,5]    R37       -1
    x[0,5]    R49       -1
    x[1,0]    R1        1
    x[1,0]    R8        1
    x[1,0]    R10       1
    x[1,0]    R26       -1
    x[1,1]    R1        1
    x[1,1]    R9        1
    x[1,1]    R11       1
    x[1,1]    R27       -1
    x[1,2]    R1        1
    x[1,2]    R12       1
    x[1,2]    R16       1
    x[1,2]    R28       -1
    x[1,3]    R1        1
    x[1,3]    R13       1
    x[1,3]    R17       1
    x[1,3]    R29       -1
    x[1,3]    R105      1
    x[1,4]    R1        1
    x[1,4]    R14       1
    x[1,4]    R18       1
    x[1,4]    R30       -1
    x[1,4]    R105      1
    x[1,5]    R1        1
    x[1,5]    R15       1
    x[1,5]    R19       1
    x[1,5]    R31       -1
    x[1,5]    R105      1
    x[2,0]    R2        1
    x[2,0]    R8        1
    x[2,0]    R10       1
    x[2,0]    R38       -1
    x[2,1]    R2        1
    x[2,1]    R9        1
    x[2,1]    R11       1
    x[2,1]    R39       -1
    x[2,2]    R2        1
    x[2,2]    R12       1
    x[2,2]    R16       1
    x[2,2]    R40       -1
    x[2,3]    R2        1
    x[2,3]    R13       1
    x[2,3]    R17       1
    x[2,3]    R41       -1
    x[2,3]    R106      1
    x[2,4]    R2        1
    x[2,4]    R14       1
    x[2,4]    R18       1
    x[2,4]    R42       -1
    x[2,4]    R106      1
    x[2,5]    R2        1
    x[2,5]    R15       1
    x[2,5]    R19       1
    x[2,5]    R43       -1
    x[2,5]    R106      1
    x[3,0]    R3        1
    x[3,0]    R8        1
    x[3,0]    R10       1
    x[3,0]    R50       -1
    x[3,0]    R56       -1
    x[3,0]    R68       -1
    x[3,0]    R107      1
    x[3,1]    R3        1
    x[3,1]    R9        1
    x[3,1]    R11       1
    x[3,1]    R51       -1
    x[3,1]    R57       -1
    x[3,1]    R69       -1
    x[3,1]    R107      1
    x[3,2]    R3        1
    x[3,2]    R12       1
    x[3,2]    R16       1
    x[3,2]    R52       -1
    x[3,2]    R58       -1
    x[3,2]    R70       -1
    x[3,3]    R3        1
    x[3,3]    R13       1
    x[3,3]    R17       1
    x[3,3]    R53       -1
    x[3,3]    R59       -1
    x[3,3]    R71       -1
    x[3,4]    R3        1
    x[3,4]    R14       1
    x[3,4]    R18       1
    x[3,4]    R54       -1
    x[3,4]    R60       -1
    x[3,4]    R72       -1
    x[3,5]    R3        1
    x[3,5]    R15       1
    x[3,5]    R19       1
    x[3,5]    R55       -1
    x[3,5]    R61       -1
    x[3,5]    R73       -1
    x[4,0]    R4        1
    x[4,0]    R8        1
    x[4,0]    R10       1
    x[4,0]    R62       -1
    x[4,1]    R4        1
    x[4,1]    R9        1
    x[4,1]    R11       1
    x[4,1]    R63       -1
    x[4,2]    R4        1
    x[4,2]    R12       1
    x[4,2]    R16       1
    x[4,2]    R64       -1
    x[4,3]    R4        1
    x[4,3]    R13       1
    x[4,3]    R17       1
    x[4,3]    R65       -1
    x[4,3]    R108      1
    x[4,4]    R4        1
    x[4,4]    R14       1
    x[4,4]    R18       1
    x[4,4]    R66       -1
    x[4,4]    R108      1
    x[4,5]    R4        1
    x[4,5]    R15       1
    x[4,5]    R19       1
    x[4,5]    R67       -1
    x[4,5]    R108      1
    x[5,0]    R5        1
    x[5,0]    R8        1
    x[5,0]    R10       1
    x[5,0]    R74       -1
    x[5,0]    R80       -1
    x[5,0]    R92       -1
    x[5,0]    R109      1
    x[5,1]    R5        1
    x[5,1]    R9        1
    x[5,1]    R11       1
    x[5,1]    R75       -1
    x[5,1]    R81       -1
    x[5,1]    R93       -1
    x[5,1]    R109      1
    x[5,2]    R5        1
    x[5,2]    R12       1
    x[5,2]    R16       1
    x[5,2]    R76       -1
    x[5,2]    R82       -1
    x[5,2]    R94       -1
    x[5,3]    R5        1
    x[5,3]    R13       1
    x[5,3]    R17       1
    x[5,3]    R77       -1
    x[5,3]    R83       -1
    x[5,3]    R95       -1
    x[5,4]    R5        1
    x[5,4]    R14       1
    x[5,4]    R18       1
    x[5,4]    R78       -1
    x[5,4]    R84       -1
    x[5,4]    R96       -1
    x[5,5]    R5        1
    x[5,5]    R15       1
    x[5,5]    R19       1
    x[5,5]    R79       -1
    x[5,5]    R85       -1
    x[5,5]    R97       -1
    x[6,0]    R6        1
    x[6,0]    R8        1
    x[6,0]    R10       1
    x[6,0]    R86       -1
    x[6,1]    R6        1
    x[6,1]    R9        1
    x[6,1]    R11       1
    x[6,1]    R87       -1
    x[6,2]    R6        1
    x[6,2]    R12       1
    x[6,2]    R16       1
    x[6,2]    R88       -1
    x[6,3]    R6        1
    x[6,3]    R13       1
    x[6,3]    R17       1
    x[6,3]    R89       -1
    x[6,3]    R110      1
    x[6,4]    R6        1
    x[6,4]    R14       1
    x[6,4]    R18       1
    x[6,4]    R90       -1
    x[6,4]    R110      1
    x[6,5]    R6        1
    x[6,5]    R15       1
    x[6,5]    R19       1
    x[6,5]    R91       -1
    x[6,5]    R110      1
    x[7,0]    R7        1
    x[7,0]    R8        1
    x[7,0]    R10       1
    x[7,0]    R98       -1
    x[7,1]    R7        1
    x[7,1]    R9        1
    x[7,1]    R11       1
    x[7,1]    R99       -1
    x[7,2]    R7        1
    x[7,2]    R12       1
    x[7,2]    R16       1
    x[7,2]    R100      -1
    x[7,2]    R111      1
    x[7,3]    R7        1
    x[7,3]    R13       1
    x[7,3]    R17       1
    x[7,3]    R101      -1
    x[7,4]    R7        1
    x[7,4]    R14       1
    x[7,4]    R18       1
    x[7,4]    R102      -1
    x[7,5]    R7        1
    x[7,5]    R15       1
    x[7,5]    R19       1
    x[7,5]    R103      -1
    y[0,0]    OBJ       -1
    y[0,0]    R20       1
    y[0,0]    R26       1
    y[0,1]    OBJ       -1
    y[0,1]    R21       1
    y[0,1]    R27       1
    y[0,2]    OBJ       -1
    y[0,2]    R22       1
    y[0,2]    R28       1
    y[0,3]    OBJ       -1
    y[0,3]    R23       1
    y[0,3]    R29       1
    y[0,4]    OBJ       -1
    y[0,4]    R24       1
    y[0,4]    R30       1
    y[0,5]    OBJ       -1
    y[0,5]    R25       1
    y[0,5]    R31       1
    y[1,0]    OBJ       -1
    y[1,0]    R32       1
    y[1,0]    R38       1
    y[1,1]    OBJ       -1
    y[1,1]    R33       1
    y[1,1]    R39       1
    y[1,2]    OBJ       -1
    y[1,2]    R34       1
    y[1,2]    R40       1
    y[1,3]    OBJ       -1
    y[1,3]    R35       1
    y[1,3]    R41       1
    y[1,4]    OBJ       -1
    y[1,4]    R36       1
    y[1,4]    R42       1
    y[1,5]    OBJ       -1
    y[1,5]    R37       1
    y[1,5]    R43       1
    y[2,0]    OBJ       -1
    y[2,0]    R44       1
    y[2,0]    R50       1
    y[2,1]    OBJ       -1
    y[2,1]    R45       1
    y[2,1]    R51       1
    y[2,2]    OBJ       -1
    y[2,2]    R46       1
    y[2,2]    R52       1
    y[2,3]    OBJ       -1
    y[2,3]    R47       1
    y[2,3]    R53       1
    y[2,4]    OBJ       -1
    y[2,4]    R48       1
    y[2,4]    R54       1
    y[2,5]    OBJ       -1
    y[2,5]    R49       1
    y[2,5]    R55       1
    y[3,0]    OBJ       -1
    y[3,0]    R56       1
    y[3,0]    R62       1
    y[3,1]    OBJ       -1
    y[3,1]    R57       1
    y[3,1]    R63       1
    y[3,2]    OBJ       -1
    y[3,2]    R58       1
    y[3,2]    R64       1
    y[3,3]    OBJ       -1
    y[3,3]    R59       1
    y[3,3]    R65       1
    y[3,4]    OBJ       -1
    y[3,4]    R60       1
    y[3,4]    R66       1
    y[3,5]    OBJ       -1
    y[3,5]    R61       1
    y[3,5]    R67       1
    y[4,0]    OBJ       -1
    y[4,0]    R68       1
    y[4,0]    R74       1
    y[4,1]    OBJ       -1
    y[4,1]    R69       1
    y[4,1]    R75       1
    y[4,2]    OBJ       -1
    y[4,2]    R70       1
    y[4,2]    R76       1
    y[4,3]    OBJ       -1
    y[4,3]    R71       1
    y[4,3]    R77       1
    y[4,4]    OBJ       -1
    y[4,4]    R72       1
    y[4,4]    R78       1
    y[4,5]    OBJ       -1
    y[4,5]    R73       1
    y[4,5]    R79       1
    y[5,0]    OBJ       -1
    y[5,0]    R80       1
    y[5,0]    R86       1
    y[5,1]    OBJ       -1
    y[5,1]    R81       1
    y[5,1]    R87       1
    y[5,2]    OBJ       -1
    y[5,2]    R82       1
    y[5,2]    R88       1
    y[5,3]    OBJ       -1
    y[5,3]    R83       1
    y[5,3]    R89       1
    y[5,4]    OBJ       -1
    y[5,4]    R84       1
    y[5,4]    R90       1
    y[5,5]    OBJ       -1
    y[5,5]    R85       1
    y[5,5]    R91       1
    y[6,0]    OBJ       -1
    y[6,0]    R92       1
    y[6,0]    R98       1
    y[6,1]    OBJ       -1
    y[6,1]    R93       1
    y[6,1]    R99       1
    y[6,2]    OBJ       -1
    y[6,2]    R94       1
    y[6,2]    R100      1
    y[6,3]    OBJ       -1
    y[6,3]    R95       1
    y[6,3]    R101      1
    y[6,4]    OBJ       -1
    y[6,4]    R96       1
    y[6,4]    R102      1
    y[6,5]    OBJ       -1
    y[6,5]    R97       1
    y[6,5]    R103      1
    MARKER    'MARKER'                 'INTEND'
RHS
    RHS1      R0        1
    RHS1      R1        1
    RHS1      R2        1
    RHS1      R3        1
    RHS1      R4        1
    RHS1      R5        1
    RHS1      R6        1
    RHS1      R7        1
    RHS1      R10       2
    RHS1      R11       2
    RHS1      R16       1
    RHS1      R17       2
    RHS1      R18       2
    RHS1      R19       2
    RHS1      R104      1
    RHS1      R105      1
    RHS1      R106      1
    RHS1      R107      1
    RHS1      R108      1
    RHS1      R109      1
    RHS1      R110      1
    RHS1      R111      1
BOUNDS
 BV BND1      x[0,0]  
 BV BND1      x[0,1]  
 BV BND1      x[0,2]  
 BV BND1      x[0,3]  
 BV BND1      x[0,4]  
 BV BND1      x[0,5]  
 BV BND1      x[1,0]  
 BV BND1      x[1,1]  
 BV BND1      x[1,2]  
 BV BND1      x[1,3]  
 BV BND1      x[1,4]  
 BV BND1      x[1,5]  
 BV BND1      x[2,0]  
 BV BND1      x[2,1]  
 BV BND1      x[2,2]  
 BV BND1      x[2,3]  
 BV BND1      x[2,4]  
 BV BND1      x[2,5]  
 BV BND1      x[3,0]  
 BV BND1      x[3,1]  
 BV BND1      x[3,2]  
 BV BND1      x[3,3]  
 BV BND1      x[3,4]  
 BV BND1      x[3,5]  
 BV BND1      x[4,0]  
 BV BND1      x[4,1]  
 BV BND1      x[4,2]  
 BV BND1      x[4,3]  
 BV BND1      x[4,4]  
 BV BND1      x[4,5]  
 BV BND1      x[5,0]  
 BV BND1      x[5,1]  
 BV BND1      x[5,2]  
 BV BND1      x[5,3]  
 BV BND1      x[5,4]  
 BV BND1      x[5,5]  
 BV BND1      x[6,0]  
 BV BND1      x[6,1]  
 BV BND1      x[6,2]  
 BV BND1      x[6,3]  
 BV BND1      x[6,4]  
 BV BND1      x[6,5]  
 BV BND1      x[7,0]  
 BV BND1      x[7,1]  
 BV BND1      x[7,2]  
 BV BND1      x[7,3]  
 BV BND1      x[7,4]  
 BV BND1      x[7,5]  
 BV BND1      y[0,0]  
 BV BND1      y[0,1]  
 BV BND1      y[0,2]  
 BV BND1      y[0,3]  
 BV BND1      y[0,4]  
 BV BND1      y[0,5]  
 BV BND1      y[1,0]  
 BV BND1      y[1,1]  
 BV BND1      y[1,2]  
 BV BND1      y[1,3]  
 BV BND1      y[1,4]  
 BV BND1      y[1,5]  
 BV BND1      y[2,0]  
 BV BND1      y[2,1]  
 BV BND1      y[2,2]  
 BV BND1      y[2,3]  
 BV BND1      y[2,4]  
 BV BND1      y[2,5]  
 BV BND1      y[3,0]  
 BV BND1      y[3,1]  
 BV BND1      y[3,2]  
 BV BND1      y[3,3]  
 BV BND1      y[3,4]  
 BV BND1      y[3,5]  
 BV BND1      y[4,0]  
 BV BND1      y[4,1]  
 BV BND1      y[4,2]  
 BV BND1      y[4,3]  
 BV BND1      y[4,4]  
 BV BND1      y[4,5]  
 BV BND1      y[5,0]  
 BV BND1      y[5,1]  
 BV BND1      y[5,2]  
 BV BND1      y[5,3]  
 BV BND1      y[5,4]  
 BV BND1      y[5,5]  
 BV BND1      y[6,0]  
 BV BND1      y[6,1]  
 BV BND1      y[6,2]  
 BV BND1      y[6,3]  
 BV BND1      y[6,4]  
 BV BND1      y[6,5]  
ENDATA
