gaussian
int_gaussian_33_1_700
results/int_gaussian_33_1_700/solution1/syn/report/int_gaussian_csynth.rpt
BENCH, UNROLL_FACTOR, LUTs, FF, DSP, BRAM,FREQ, Latency, II,
int_gaussian, 33, 8801, 43381, 891, 0, 1.659,
+ Latency: 
    * Summary: 
    +---------+---------+-----------+-----------+-----+-----+---------+
    |  Latency (cycles) |   Latency (absolute)  |  Interval | Pipeline|
    |   min   |   max   |    min    |    max    | min | max |   Type  |
    +---------+---------+-----------+-----------+-----+-----+---------+
    |       46|       46| 65.734 ns | 65.734 ns |   46|   46|   none  |
    +---------+---------+-----------+-----------+-----+-----+---------+

--
        |          |  Latency (cycles) | Iteration|  Initiation Interval  | Trip |          |
        | Loop Name|   min   |   max   |  Latency |  achieved |   target  | Count| Pipelined|
        +----------+---------+---------+----------+-----------+-----------+------+----------+
        |- loop1   |       44|       44|        14|          1|          1|    31|    yes   |
        +----------+---------+---------+----------+-----------+-----------+------+----------+


