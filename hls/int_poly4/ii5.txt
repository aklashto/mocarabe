int_poly4
int_poly4_585_4095_700
results/int_poly4_585_4095_700/solution1/syn/report/int_poly4_csynth.rpt
BENCH, UNROLL_FACTOR, LUTs, FF, DSP, BRAM,FREQ, Latency, II,
int_poly4, 585, 68647, 373499, 6105, 0, 6.069,
+ Latency: 
    * Summary: 
    +---------+---------+-----------+-----------+-----+-----+---------+
    |  Latency (cycles) |   Latency (absolute)  |  Interval | Pipeline|
    |   min   |   max   |    min    |    max    | min | max |   Type  |
    +---------+---------+-----------+-----------+-----+-----+---------+
    |       42|       42| 60.018 ns | 60.018 ns |   42|   42|   none  |
    +---------+---------+-----------+-----------+-----+-----+---------+

--
        |          |  Latency (cycles) | Iteration|  Initiation Interval  | Trip |          |
        | Loop Name|   min   |   max   |  Latency |  achieved |   target  | Count| Pipelined|
        +----------+---------+---------+----------+-----------+-----------+------+----------+
        |- loop1   |       40|       40|        36|          5|          5|     1|    yes   |
        +----------+---------+---------+----------+-----------+-----------+------+----------+


