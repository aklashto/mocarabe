// Example parameters: compiler will generate precise parameters
`define BENCHMARK_DATA_WIDTH 32
`define D_WIDTH [`BENCHMARK_DATA_WIDTH-1:0]
`define BENCHMARK_X_WIDTH 2
`define BENCHMARK_Y_WIDTH 2
`define BENCHMARK_SCHED_LEN 16
`define BENCHMARK_CHAN_WIDTH 2
`define FIFO_DEPTH 16
`define TORUS_SWITCH_PIPE_NUM   2
